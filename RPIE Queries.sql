/* Acknowledgements */
SELECT ProjectName, WantsAcknowledgement
, CASE WHEN SubmittedDate IS NULL THEN 'Filing Started' ELSE 'Filing Submitted' END AS [Status]
, CONCAT(Party.FirstName, ' ', Party.LastName) AS [Party Name]
, CONCAT(Address.StreetNumber, ' ', Address.StreetName, ' ', Address.StreetSuffix, ' ', Address.City, ' ', Address.State, Address.ZipCode) AS [Filing Address]
, Pin.PinNumber, SubmittedDate
, Party.PartyTypeCode, Party.Email, Party.PrimaryPhone, Party.PartyTypeOtherDescription
FROM Building AS A
INNER JOIN
FilingBuilding AS B
ON A.BuildingId=B.BuildingId
INNER JOIN FilingBuildingAddress
ON FilingBuildingAddress.BuildingId=B.BuildingId
INNER JOIN Address
ON FilingBuildingAddress.AddressId=Address.AddressId
INNER JOIN Filing AS C
ON B.FilingId=C.FilingId
INNER JOIN
FilingIncomeExpense AS F
ON C.FilingId=F.FilingId
INNER JOIN FilingParty
ON C.FilingId=FilingParty.FilingId
INNER JOIN Party
ON FilingParty.PartyId=Party.PartyId
INNER JOIN
FilingPin
ON C.FilingId=FilingPin.FilingId
INNER JOIN 
Pin
ON FilingPin.PinId=Pin.PinId
WHERE (1=1) 
AND NoticeId NOT IN ('999-999-999-9') 
AND WantsAcknowledgement=1


/* Spaces associated with completed filings */
SELECT ProjectName, PinNumber, WantsAcknowledgement, G.ScheduleELine23E, G.ScheduleELine24, G.IsLesseeResponsibleTaxes, D.*, E.*
FROM Building AS A
INNER JOIN
FilingBuilding AS B
ON A.BuildingId=B.BuildingId
INNER JOIN Filing AS C
ON B.FilingId=C.FilingId
INNER JOIN
FilingIncomeExpense AS F
ON C.FilingId=F.FilingId
INNER JOIN  
IncomeExpense AS G
ON F.IncomeExpenseId=G.IncomeExpenseId
INNER JOIN
FilingPin
ON C.FilingId=FilingPin.FilingId
INNER JOIN 
Pin
ON FilingPin.PinId=Pin.PinId
LEFT JOIN CommercialSpace AS D
ON A.BuildingId=D.BuildingId
LEFT JOIN ResidentialSpace AS E
ON A.BuildingId=E.BuildingId
WHERE NoticeId NOT IN ('999-999-999-9') 
/* AND PinNumber='15284050130000' */
 AND SubmittedDate IS NOT NULL 
 
/* AND NoticeId='RNX-1B-VWO-7'*/

/* Called 'RD2-2LL-3A3-6' and left a message 1467 RING */


/* Contact Filer */

SELECT Party.* 
FROM Filing
INNER JOIN FilingParty
ON Filing.FilingId=FilingParty.FilingId
INNER JOIN Party
ON FilingParty.PartyId=Party.PartyId
Where (1=1) 
/* ND NoticeId='OQX-58E-3RY-C' */
AND  NoticeId NOT IN ('999-999-999-9') 
ORDER BY LastName

/* Total Filings */
SELECT COUNT(DISTINCT Filing.FilingId) As [Filings Created]
, COUNT(DISTINCT SubmittedDate) AS [Filings Submitted]
, Count(DISTINCT Pin.PinId) AS [PINs]
FROM Filing 
INNER JOIN
FilingPin
ON Filing.FilingId=FilingPin.FilingId
INNER JOIN 
Pin
ON FilingPin.PinId=Pin.PinId
WHERE NoticeId NOT IN ('999-999-999-9')