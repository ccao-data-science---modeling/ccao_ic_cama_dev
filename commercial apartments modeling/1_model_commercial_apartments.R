# Source necessary scripts. Load objects. 
source("commercial apartments modeling/0_fetch_data.R")

# Explore the data. USe log coordinates transformations. 
ggplot(data = commercial_space) +
  geom_point(aes(x = SquareFeet, y = RentPerMonth, color = UseDescription, shape = LeaseTypeCode)) + 
  coord_trans(x = "log2", y = "log2") 

# Explore relationship between square fett and rent per month
ggplot(data = commercial_space, aes(SquareFeet, RentPerMonth, UseDescription)) +
  geom_point() + 
  coord_trans(x = "log2", y = "log2") + 
  geom_smooth(method = "lm")

# Relationship seems to be linear.
# Multiple linear regression. Assumes normal distribution of data. 
multiple_linear_model <- lm(RentPerMonth ~ SquareFeet + UseDescription + IsOwnerOccupied + median_rent + median_income_total, data = commercial_space)
summary(multiple_linear_model)
# Excellent results. Ajusted R-squared of 0.9065.
# Significant variables: Square feet, median rent per zip. Median income per zip.
# Plot distribution of residuals
data <- augment(multiple_linear_model)
ggplot(data, aes(x = .fitted, y = .resid)) + 
  geom_point()

# Log transformationm for multiple linear regression. 
commercial_space_log <- commercial_space %>%
  mutate(log_rent = log2(RentPerMonth), 
         log_feet = log2(SquareFeet))
# Plot log transformation. Relationship seems linear. 
ggplot(data = commercial_space_log, aes(log_rent, log_feet)) +
  geom_point() +
  geom_smooth(method = "lm")

# Log multiple linear regression. 
log_multiple_linear_model <- lm(log_rent ~ log_feet + UseDescription + IsOwnerOccupied + median_rent + median_income_total, data = commercial_space_log)
summary(log_multiple_linear_model)
# R square is smaller that before. But user description becomes significant. At the lower level of the 
# distribution offices are significantly more expensive than other bussiness.