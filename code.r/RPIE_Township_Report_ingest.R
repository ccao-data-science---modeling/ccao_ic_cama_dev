# retrieve township name for input township code
township_name <- as.character(dbGetQuery(CCAODATA, paste0("
  SELECT township_name FROM FTBL_TOWNCODES

  WHERE township_code = ", params$township, "
  ")))

# graph data ----

# retrieve spaces data
graph_data <- dbGetQuery(RPIE, paste0("
  SELECT PinNumber AS PIN,
  ResidentialSpaceId, RS.BedroomCount AS Bedrooms, RS.RentPerMonth AS [Residential Rent], RS.SquareFeet AS [Residential SQFT],
  CommercialSpaceId, CS.UseDescription AS [Use Type], CS.RentPerMonth AS [Commercial Rent], CS.SquareFeet AS [Commercial SQFT]

  FROM Filing F

  LEFT JOIN FilingPinBuilding FPB ON F.FilingId = FPB.FilingId

  LEFT JOIN Pin P ON FPB.PinId = P.PinId

  LEFT JOIN
      (SELECT PIN, LEFT(HD_TOWN, 2) AS Township FROM [CCAODATASRV].[CCAODATA].[dbo].[AS_HEADT]
      WHERE TAX_YEAR = YEAR(GETDATE()) - 1) AS H
      ON P.PinNumber = H.PIN

  LEFT JOIN ResidentialSpace RS ON FPB.BuildingId = RS.BuildingId

  LEFT JOIN CommercialSpace CS ON FPB.BuildingId = CS.BuildingId

  WHERE NoticeId != '999-999-999-9'
    AND Township = '", params$township, "'
  "))

# residential spaces - remove duplicates cause by joining with commercial spaces, remove spaces with missing info
res_graph_data <- graph_data[, 1:5] %>%
  filter(!duplicated(ResidentialSpaceId)
         & !is.na(`Residential Rent`)
         & !is.na(`Residential SQFT`)
         & between(`Residential Rent`, 1, 4999)
         & `Residential SQFT` < 2000) %>%
  mutate("Residential Rent per SQFT" = `Residential Rent` / `Residential SQFT`)

com_graph_data <- graph_data[, c(1, 6, 7, 8, 9)] %>%
  filter(!duplicated(CommercialSpaceId)
         & !is.na(`Commercial Rent`)
         & !is.na(`Commercial SQFT`)
         & between(`Commercial Rent`, 1, 19999)) %>%
  mutate("Commercial Rent per SQFT" = `Commercial Rent` / `Commercial SQFT`)

# standardize column names
colnames(res_graph_data) <- gsub("Residential ", "", colnames(res_graph_data))
colnames(com_graph_data) <- gsub("Commercial ", "", colnames(com_graph_data))

# map data ----

map_data <- inner_join(

  # lat/long lets us place RPIE data on a map
  dbGetQuery(CCAODATA, paste0("
    SELECT HEAD.PIN, NBHD, centroid_y AS LAT, centroid_x AS LONG

    FROM DTBL_PINLOCATIONS LOX

    RIGHT JOIN
    (SELECT PIN, LEFT(HD_TOWN, 2) AS TOWNSHIP, HD_NBHD AS NBHD, TAX_YEAR FROM AS_HEADT
      WHERE TAX_YEAR = YEAR(GETDATE()) AND LEFT(HD_CLASS, 1) = 3) HEAD
    ON LEFT(LOX.PIN, 10) = LEFT(HEAD.PIN, 10)

    WHERE TOWNSHIP = '", params$township, "'
    ")),

  # address data from RPIE can be less complete, but is much faster to retrieve
  dbGetQuery(RPIE, "
    SELECT DISTINCT [PinNumber] AS PIN,
    REPLACE(REPLACE(UPPER(CONCAT(TRIM(StreetNumber), ' ',
    TRIM(StreetDirection), ' ',
    TRIM(StreetName), ' ',
    TRIM(StreetType), ' ',
    TRIM(StreetSuffix), ' ',
    TRIM(UnitNumber), ' ',
    TRIM(City), CASE WHEN [State] IS NOT NULL THEN ', ' ELSE '' END,
    TRIM([State]))), '  ', ' '), '  ', ' ') AS [Address]

    FROM FilingPin FP

    LEFT JOIN Filing F ON FP.FilingId = F.FilingId

    LEFT JOIN Pin P ON FP.PinId = P.PinId

    LEFT JOIN FilingPinBuilding FPB ON F.FilingId = FPB.FilingId AND P.PinId = FPB.PinId

    LEFT JOIN FilingBuildingAddress FBA ON F.FilingId = FBA.FilingId AND FPB.BuildingId = FBA.BuildingId

    LEFT JOIN [Address] A ON FBA.AddressId = A.AddressId

    WHERE NoticeId != '999-999-999-9'
    ") %>% filter(duplicated(PIN) == FALSE),

  by = "PIN")

# formatting map data and converting to a spatial data frame for leaflet
map_data[map_data == "  , "] <- ""
map_data$Address <- gsub(" ,", "", map_data$Address)
map_data <- st_as_sf(map_data, coords = c("LONG", "LAT"), crs = 4326)

# retrieve residential rent data at PIN level
res_rent <- dbGetQuery(RPIE, "
  SELECT PinNumber AS PIN, SUM(RentPerMonth) / SUM(SquareFeet) AS [Rent per Square Foot]

  FROM ResidentialSpace RS

  LEFT JOIN FilingPinBuilding FPB ON RS.BuildingId = FPB.BuildingId

  LEFT JOIN Pin P ON FPB.PinId = P.PinId

  LEFT JOIN Filing F ON FPB.FilingId = F.FilingId

  WHERE RentPerMonth IS NOT NULL
      AND SquareFeet IS NOT NULL
      AND NoticeId != '999-999-999-9'

  GROUP BY PinNumber
  ")

# combine residential PINs with geographic data
res_map <- inner_join(map_data, res_rent, by = "PIN")

# residential leaflet map
res_popup <- paste0(
  "<b> PIN: </b>", pin_format_pretty(res_map$PIN),
  "<br/> <b> Neighborhood: </b>", res_map$NBHD,
  "<br/> <b> Address: </b>", res_map$Address,
  "<br/> <b> Price per Square Foot: </b>", paste0("$", formatC(res_map$`Rent per Square Foot`, format="f", big.mark=",", digits=2)))

res_bins <- c(0, 0.5, 1, 1.5, 2, max(res_map$`Rent per Square Foot`, na.rm = TRUE))
res_pal <- colorBin("Blues", domain = res_map$`Rent per Square Foot`, bins = res_bins, na.color = "#CCCCCC")

# retrieve commercial rent data at PIN level
com_rent <- dbGetQuery(RPIE, "
  SELECT PinNumber AS PIN, SUM(RentPerMonth) / SUM(SquareFeet) AS [Rent per Square Foot]

  FROM CommercialSpace CS

  LEFT JOIN FilingPinBuilding FPB ON CS.BuildingId = FPB.BuildingId

  LEFT JOIN Pin P ON FPB.PinId = P.PinId

  LEFT JOIN Filing F ON FPB.FilingId = F.FilingId

  WHERE RentPerMonth IS NOT NULL
      AND SquareFeet IS NOT NULL
      AND NoticeId != '999-999-999-9'

  GROUP BY PinNumber
  ")

# combine commercial PINs with geographic data
com_map <- inner_join(map_data, com_rent, by = "PIN")

# commercial leaflet map
com_popup <- paste0(
  "<b> PIN: </b>", pin_format_pretty(com_map$PIN),
  "<br/> <b> Neighborhood: </b>", com_map$NBHD,
  "<br/> <b> Addcoms: </b>", com_map$Addcoms,
  "<br/> <b> Price per Square Foot: </b>", paste0("$", formatC(com_map$`Rent per Square Foot`, format="f", big.mark=",", digits=2)))

com_bins <- c(0.5, 1, 1.5, 2, 2.5, 3, max(com_map$`Rent per Square Foot`, na.rm = TRUE))
com_pal <- colorBin("Blues", domain = com_map$`Rent per Square Foot`, bins = com_bins, na.color = "#CCCCCC")

# table data ----

# retrieve data about spaces, filings, PINs, and appeals
spaces <- dbGetQuery(RPIE, paste0("
  SELECT PIN, FPB.BuildingId, Appealed, [Appeal Status], Township, SubmittedDate, ResidentialSpaceId, CommercialSpaceId,
  RS.RentPerMonth AS [Residential Rent], RS.SquareFeet AS [Residential SQFT], RS.Vacancy AS [Residential Vacancy],
  CS.RentPerMonth AS [Commercial Rent], CS.SquareFeet AS [Commercial SQFT], CS.Vacancy AS [Commercial Vacancy],
  CASE WHEN F.FilingId IS NOT NULL THEN 1 ELSE 0 END AS RPIE

  FROM Filing F

  LEFT JOIN FilingPin FP ON F.FilingId = FP.FilingId

  LEFT JOIN FilingPinBuilding FPB ON FP.FilingId = FPB.FilingId AND FP.PinId = FPB.PinId

  LEFT JOIN Pin P ON FP.PinId = P.PinId

  FULL JOIN
      (SELECT HEAD.PIN, LEFT(HD_TOWN, 2) AS Township, LEFT(HD_CLASS, 1) AS CLASS,
      CASE WHEN APPEALS.PIN IS NOT NULL THEN 1 ELSE 0 END AS Appealed,
      CASE WHEN PC_DK_RESULT_1 = 'C' OR  PC_DK_RESULT_2 = 'C' OR PC_DK_RESULT_3 = 'C' THEN 1
      ELSE NULL END AS [Appeal Status]
      FROM [CCAODATASRV].[CCAODATA].[dbo].[AS_HEADT] HEAD

      LEFT JOIN [CCAODATASRV].[CCAODATA].[dbo].[APPEALSDATA] APPEALS ON HEAD.PIN = APPEALS.PIN AND HEAD.TAX_YEAR = APPEALS.TAX_YEAR
      WHERE HEAD.TAX_YEAR = YEAR(GETDATE())
        AND LEFT(HD_TOWN, 2) = '", params$township, "'
        AND LEFT(HD_CLASS, 1) = 3) A
  ON P.PinNumber = A.PIN

  LEFT JOIN ResidentialSpace RS ON FPB.BuildingId = RS.BuildingId

  LEFT JOIN CommercialSpace CS ON FPB.BuildingId = CS.BuildingId

  WHERE (NoticeId != '999-999-999-9' OR NoticeId IS NULL)
    AND Township = '", params$township, "'
    AND CLASS = 3
  ")) %>%
  mutate("Residential Rent" = ifelse(duplicated(ResidentialSpaceId), NA, `Residential Rent`),
         "Residential SQFT" = ifelse(duplicated(ResidentialSpaceId), NA, `Residential SQFT`),
         "Commercial Rent" = ifelse(duplicated(CommercialSpaceId), NA, `Commercial Rent`),
         "Commercial SQFT" = ifelse(duplicated(CommercialSpaceId), NA, `Commercial SQFT`))

# Town level PIN information
township_pins <- c("Total 300 Class PINs in township" = length(unique(spaces$PIN)),
                   "Total 300 Class PINs with Appeals" = length(unique(spaces$PIN[spaces$Appealed == 1])),
                   "Appealed PINs with RPIE Filings" = nrow(spaces[spaces$Appealed == 1 & spaces$RPIE == 1 & !duplicated(spaces$PIN), ]),
                   "Appealed PINs with RPIE Spaces" =
                     length(unique(spaces$PIN[!is.na(spaces$`Residential Rent`) | !is.na(spaces$`Commercial Rent`)])))

# generate residential vacancy data for graph
res_vacancy <- spaces %>%
  mutate("Vacancy" = case_when(`Residential Vacancy` == '0' ~ 0,
                               `Residential Vacancy` == '0-2' ~ 1,
                               `Residential Vacancy` == '2-4' ~ 3,
                               `Residential Vacancy` == '4-6' ~ 5,
                               `Residential Vacancy` == '6-8' ~ 7,
                               `Residential Vacancy` == '8-10' ~ 9,
                               `Residential Vacancy` == '10-12' ~ 11,),
         "Rent Per SQFT" = ifelse(!is.na(`Residential Rent`) & !is.na(`Residential SQFT`),
                                  `Residential Rent` / `Residential SQFT`, NA)) %>%
  filter(!duplicated(ResidentialSpaceId) & !is.na(ResidentialSpaceId) & `Rent Per SQFT` < 5) %>%
  select("ResidentialSpaceId", "Vacancy", "Rent Per SQFT")

# generate commercial vacancy data for graph
com_vacancy <- spaces %>%
  mutate("Vacancy" = case_when(`Commercial Vacancy` == '0' ~ 0,
                               `Commercial Vacancy` == '0-2' ~ 1,
                               `Commercial Vacancy` == '2-4' ~ 3,
                               `Commercial Vacancy` == '4-6' ~ 5,
                               `Commercial Vacancy` == '6-8' ~ 7,
                               `Commercial Vacancy` == '8-10' ~ 9,
                               `Commercial Vacancy` == '10-12' ~ 11,),
         "Rent Per SQFT" = ifelse(!is.na(`Commercial Rent`) & !is.na(`Commercial SQFT`),
                                  `Commercial Rent` / `Commercial SQFT`, NA)) %>%
  filter(!duplicated(CommercialSpaceId) & !is.na(CommercialSpaceId) & `Rent Per SQFT` < 5) %>%
  select("CommercialSpaceId", "Vacancy", "Rent Per SQFT")

# group spaces by PIN and format output for table
spaces <- spaces %>%
  group_by(PIN) %>%
  summarise("Appealed" = sum(Appealed, na.rm = TRUE),
            "Appeal Status" = sum(`Appeal Status`, na.rm = TRUE),
            "RPIE" = sum(RPIE, na.rm = TRUE),
            "Residential N" = ifelse(sum(!is.na(`Residential Rent`)) > 0, sum(!is.na(`Residential Rent`)), NA),
            "Residential Rent" = sum(`Residential Rent`, na.rm = TRUE),
            "Residential SQFT" = ifelse(sum(`Residential SQFT`, na.rm = TRUE) > 0, sum(`Residential SQFT`, na.rm = TRUE), NA),
            "Residential Rent Per SQFT" = ifelse(sum(`Residential SQFT`, na.rm = TRUE) > 0,
                                                 sum(`Residential Rent`, na.rm = TRUE) / sum(`Residential SQFT`, na.rm = TRUE), NA),
            "Commercial N"= ifelse(sum(!is.na(`Commercial Rent`)) > 0, sum(!is.na(`Commercial Rent`)), NA),
            "Commercial Rent" = sum(`Commercial Rent`, na.rm = TRUE),
            "Commercial SQFT" = ifelse(sum(`Commercial SQFT`, na.rm = TRUE) > 0, sum(`Commercial SQFT`, na.rm = TRUE), NA),
            "Commercial Rent Per SQFT" = ifelse(sum(`Commercial SQFT`, na.rm = TRUE) > 0,
                                                sum(`Commercial Rent`, na.rm = TRUE) / sum(`Commercial SQFT`, na.rm = TRUE), NA)) %>%
  arrange(PIN) %>%
  filter(Appealed > 0 | RPIE > 0) %>%
  mutate("PIN" = pin_format_pretty(PIN, full_length = TRUE),
         "Appealed" = ifelse(Appealed > 0, '\U2714', '\U2717'),
         "Appeal Status" = ifelse(`Appeal Status` > 0, 'WON', ''),
         "RPIE" = ifelse(RPIE > 0, '\U2714', '\U2717'),
         "Residential Rent" = ifelse(`Residential Rent` > 0,
                                     paste0("$", formatC(`Residential Rent`, format="f", big.mark=",", digits=0)), ""),
         "Commercial Rent" = ifelse(`Commercial Rent` > 0,
                                    paste0("$", formatC(`Commercial Rent`, format="f", big.mark=",", digits=0)), ""),
         "Residential Rent Per SQFT" = ifelse(!is.na(`Residential Rent Per SQFT`),
                                              paste0("$", formatC(`Residential Rent Per SQFT`, format="f", big.mark=",", digits=2)), ""),
         "Commercial Rent Per SQFT" = ifelse(!is.na(`Commercial Rent Per SQFT`),
                                             paste0("$", formatC(`Commercial Rent Per SQFT`, format="f", big.mark=",", digits=2)), ""))

colnames(spaces) <- c("PIN", "Appealed", "Appeal Status", "RPIE",
                      "N", "Rent", "SQFT", "Rent Per SQFT", "N", "Rent", "SQFT", "Rent Per SQFT")